<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    //Kategori
    Route::get('/kategori', [App\Http\Controllers\KategoriController::class, 'index'])->name('kategori');
    Route::get('/listKategori',[App\Http\Controllers\KategoriController::class,'ListKategori'])->name('list.kategori');
    Route::get('/add_kategori', [App\Http\Controllers\KategoriController::class, 'create'])->name('create.kategori');
    Route::post('/store_kategori', [App\Http\Controllers\KategoriController::class, 'store'])->name('store.kategori');
    Route::get('/edit_kategori/{id}', [App\Http\Controllers\KategoriController::class, 'edit'])->name('edit.kategori');
    Route::post('/update_kategori/{id}', [App\Http\Controllers\KategoriController::class, 'update'])->name('update.kategori');
    Route::get('/delete_kategori/{id}', [App\Http\Controllers\KategoriController::class, 'delete'])->name('delete.kategori');

    //Data Stock (pertama yang harus dibuat)
    Route::get('/data_stock', [App\Http\Controllers\DatastockController::class, 'index'])->name('index.data_stock');
    Route::get('/listDataStock',[App\Http\Controllers\DatastockController::class,'ListDataStock'])->name('list.data_stock');
    Route::get('/add_data_stock', [App\Http\Controllers\DatastockController::class, 'create'])->name('create.data_stock');
    Route::post('/store_data_stock', [App\Http\Controllers\DatastockController::class, 'store'])->name('store.data_stock');
    Route::get('/edit_data_stock/{id}', [App\Http\Controllers\DatastockController::class, 'edit'])->name('edit.data_stock');
    Route::post('/update_data_stock/{id}', [App\Http\Controllers\DatastockController::class, 'update'])->name('update.data_stock');
    Route::get('/delete_data_stocki/{id}', [App\Http\Controllers\DatastockController::class, 'delete'])->name('delete.data_stock');
    Route::get('/history_data_stock/{id}', [App\Http\Controllers\DatastockController::class, 'index_history'])->name('index.history');
    Route::post('/store_history_data_stock/{id}', [App\Http\Controllers\DatastockController::class, 'store_history'])->name('store.history');


});

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


