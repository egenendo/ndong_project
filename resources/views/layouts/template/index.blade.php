@include('layouts.navbar.index') {{-- Navbar Header --}}
@include('layouts.sidebar.sidebar') {{-- Menu Sidebar --}}
@yield('content') {{-- content --}}
@include('layouts.footer.index') {{--Footer--}}
@include('layouts.footer.js') {{--JS--}}