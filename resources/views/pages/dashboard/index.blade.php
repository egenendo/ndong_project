@extends('layouts.app',[
    'activePage'    => 'dashboard',
    'title'         => __('')
    ])

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{$kategori}}</h3>

                            <p>Kategori</p>
                        </div>
                        <div class="icon">
                            {{-- <i class="ion ion-bag"></i> --}}
                            <i class="nav-icon fas fa-list"></i>
                        </div>                        
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3>{{$data_stock}}</h3>

                            <p>Data Stock</p>
                        </div>
                        <div class="icon">
                            <i class="nav-icon fas fa-box"></i>
                        </div>                    
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                        <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>{{$history_add}}</h3>

                            <p>Barang Masuk</p>
                        </div>
                        <div class="icon">
                            <i class="nav-icon fas fa-boxes"></i>
                        </div>
                    
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>{{$history_use}}</h3>

                            <p>Barang Keluar</p>
                        </div>
                        <div class="icon">
                            <i class="nav-icon fas fa-truck-loading"></i>
                        </div>
                    
                    </div>
                </div>
                <!-- ./col -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection