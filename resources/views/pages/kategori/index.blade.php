@extends('layouts.app',[
    'activePage'    => 'kategori',
    'title'         => __('Kategori')
    ])

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Kategori</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Kategori</li>
                    </ol>
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @include('layouts.notification.partial')
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Kategori List</h3>
                            </div>
                            <!-- /.card-header -->
                            
                            <div class="card-body">
                                <a href="{{route('create.kategori')}}" class="btn btn-success btn-rounded waves-effect waves-light float-sm-right">
                                    <i class="ri-add-circle-line"></i> 
                                    <span>Add Kategori</span>
                                </a><br><br>
                                <table id="kategoriTable" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Kategori</th>
                                            <th>created at</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                            </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@push('javascript')
<script>
  $(function () {
        $("#kategoriTable").DataTable({
            processing: true,
            serverSide: false,
            ajax: "{{ route('list.kategori') }}",
            columns: [
                {
                   "data": "id_kategori",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'nama_kategori', name: 'nama_kategori'},
                {
                data: 'created_at',
                type: 'num',
                render: {
                    _: 'display',
                    sort: 'timestamp'
                    }
                },
                {data: 'action', name: 'action'},
            ]
        })
    });
</script>
@endpush