<div class="card-body">
    <div class="form-group">
        <label for="Name">Nama Kategori</label>
        <input type="text" id="nama_kategori" class="form-control @error('nama_kategori') is-invalid @enderror" name='nama_kategori' value='@if (isset($kategori->nama_kategori)) {{ $kategori->nama_kategori }} @endif' placeholder="nama_kategori">
        @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror 
    </div>
</div>
<div class="card-footer">
    <button type="submit" class="btn btn-success waves-effect waves-light me-1">Submit</button>
    <button type="button" class="btn btn-danger waves-effect" onclick="location.href='{{route('kategori')}}'">Cancel</button>
</div>