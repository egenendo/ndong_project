<div class="card-body">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="Code item">Code Item</label>
                <input type="text" class="form-control" name="" id="" value="{{$data_stock->code_item}}" disabled>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="Stock">Stock</label>
                <input type="text" class="form-control" name="" id="" value="{{$data_stock->stock}}" disabled>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="Nama Item">Nama Item</label>
                <input type="text" class="form-control" name="" id="" value="{{$data_stock->nama_item}}" disabled>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="Jenis">Jenis</label>
                <input type="text" class="form-control" name="" id="" value="{{$data_stock->nama_kategori}}" disabled>
            </div>
        </div>
        <div class="col-md-4">
            <label for="Penggunaan atau Penambahan">Jumlah Penggunaan / Penambahan</label>
            <input type="text" class="form-control @error('stock_history') is-invalid @enderror" name="stock_history" id="stock_history" value="{{old('stock_history')}}" onkeypress="return IsNumericStockHistory(event);" ondrop = "return false;" onpaste="return false;" placeholder="Input Data">
            <span id="errorStockHistory" style="color: Red; display: none">* Input Digit (0 - 9)</span>
            @error('stock_history')
                <div class="invalid-feedback" style="color: red">{{ $message }}</div>
            @enderror
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Status</label>
                    <select class="form-control" @error('status_history') is-invalid @enderror name="status_history" id="status_history">
                        {{-- <option> -- Select Status -- </option> --}}
                        <option value="0" @if(old('status_history')==0) selected @endif>Stock Digunakan (-)</option>
                        <option value="1" @if(old('status_history')==1) selected @endif>Stock Ditambahkan (+)</option>
                    </select>
                @error('status_history')
                    <div class="invalid-feedback" style="color: red">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Supplier / Admin</label>
                <input type="text" class="form-control" @error('supplier_name') is-invalid @enderror id="supplier_name" name="supplier_name" value="{{old('supplier_name')}}" placeholder="Input Supplier Name">
                @error('supplier_name')
                    <div class="invalid-feedback" style="color: red">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="col-md-12">
            <label>Description </label>
            <textarea type="text" class="form-control" @error('description') is-invalid @enderror id="description" name="description" placeholder="Keterangan Pencatatan Barang">{{old('description')}}</textarea>
            @error('description')
                <div class="invalid-feedback" style="color: red">{{ $message }}</div>
            @enderror
        </div>
    </div>
</div>
<div class="card-footer">
    <button type="submit" class="btn btn-success waves-effect waves-light me-1">Submit</button>
    <button type="button" class="btn btn-danger waves-effect" onclick="location.href='{{route('index.history',$data_stock->id_data_stock)}}'">Cancel</button>
</div>