<div class="card-body">
    <div class="form-group">
        <label for="Name">Code Item</label>
        <input type="text" id="code_item" class="form-control @error('code_item') is-invalid @enderror" name='code_item' value='@if (isset($data_stock->code_item)) {{ $data_stock->code_item }} @endif' placeholder="Code Item">
        @error('code_item')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror 
    </div>
    <div class="form-group">
        <label for="Name">Nama Item</label>
        <input type="text" id="nama_item" class="form-control @error('nama_item') is-invalid @enderror" name='nama_item' value='@if (isset($data_stock->nama_item)) {{ $data_stock->nama_item }} @endif' placeholder="Nama Item">
        @error('nama_item')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror 
    </div>
    <div class="form-group">
        <label for="Jenis Sambal">Jenis Sambal</label>
        <select class="form-control" name="id_kategori" id="id_kategori">
            <option disabled>-- Select Jenis --</option>
            @foreach ($kategori as $k)
                <option value="{{$k->id_kategori}}" @if(isset($data_stock->id_kategori)) @if($data_stock->id_kategori == $k->id_kategori) selected @else @endif  @endif>{{ $k->nama_kategori }}</option>
            @endforeach
        </select>
    </div>
    @error('id_kategori')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="Image">Images</label>
        @if (isset($data_stock->images))
            @if($data_stock->images == true)
                <div class="col-md-6">
                    <img src="{{asset($data_stock->images)}}" class="rounded me-2" width="200" alt="" data-holder-rendered="true">
                </div><br>
            @endif
        @endif
        <input type="file" id="images"class="form-control @error('images') is-invalid @enderror" name='images'>
        <p>Max Image Size 1Mb</p>
    </div>
    @error('images')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="Name">Harga</label>
        <input type="text" id="harga" class="form-control @error('harga') is-invalid @enderror" name='harga' value="@if (isset($data_stock->harga)) {{ $data_stock->harga }} @endif" onkeypress="return IsNumericHarga(event);" ondrop = "return false;" onpaste="return false;" placeholder="Harga">
        <span id="errorHarga" style="color: Red; display: none">* Input Digit (0 - 9)</span>
        @error('harga')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror 
    </div>
    <div class="form-group">
        <label for="inputName">Deskripsi Item</label>
        <textarea id="elm1" name="description" class="form-control @error('description') is-invalid @enderror" rows="10" placeholder="Message">@if(isset($data_stock->description)){{$data_stock->description}}@endif</textarea>
    </div>
    @error('description')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
</div>
<div class="card-footer">
    <button type="submit" class="btn btn-success waves-effect waves-light me-1">Submit</button>
    <button type="button" class="btn btn-danger waves-effect" onclick="location.href='{{route('index.data_stock')}}'">Cancel</button>
</div>