@extends('layouts.app',[
    'activePage'    => 'data_stock',
    'title'         => __('History Data Stock')
    ])

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">History Data Stock</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active">History Data Stock</li>
                </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('layouts.notification.partial')
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Admin Data Stock</h3>
                        </div>
                        <!-- /.card-header -->
                        <form role="form" method="post" action="{{route('store.history',$data_stock->id_data_stock)}}" enctype="multipart/form-data">
                            @csrf
                            @include('pages.datastock.formHistory')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">History Data Stock</h3>
                        </div>
                        <div class="card-body">
                            <table id="stockTable" class="table table-bordered table-hover">
                                 <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Photo</th>
                                        <th>Code Item</th>
                                        <th>Nama Item</th>
                                        <th>Harga</th>                            
                                        <th>History Data</th>
                                        <th>Description</th>
                                        <th>Supplier</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($history_item as $item)
                                        @if (isset($item))
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>
                                                    <div class="col-sm-4">
                                                        <img class="img-fluid" src="{{asset($item->images)}}" alt="{{$item->nama_item}}">
                                                    </div>
                                                </td>
                                                <td>{{$item->code_item}}</td>
                                                <td>{{$item->nama_item}}</td>
                                                <td>Rp {{currencyFormat($item->harga)}}</td>
                                                @if ($item->status_history == 1)
                                                    <td class="text-center"><span class="badge bg-success">+ {{$item->stock_history}}</span></td>
                                                @else
                                                    <td class="text-center"><span class="badge bg-danger">- {{$item->stock_history}}</span></td>
                                                @endif
                                                {{-- <td>{{$item->stock_history}}</td> --}}
                                                <td>{{$item->description}}</td>
                                                <td>{{$item->supplier_name}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@push('javascript')
    <script>
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        function IsNumericStockHistory(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
            document.getElementById("errorStockHistory").style.display = ret ? "none" : "inline";
            return ret;
        }
          $(function () {
            $('#stockTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endpush