@extends('layouts.app',[
    'activePage'    => 'data_stock',
    'title'         => __('Data Stock')
    ])

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Create Data Stock</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Create Data Stock</li>
                    </ol>
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Create Data Stock</h3>
                            </div>
                            <!-- /.card-header -->
                            <form role="form" method="post" action="{{route('store.data_stock')}}" enctype="multipart/form-data">
                                @csrf
                                @include('pages.datastock.form')
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@push('javascript')
<script>
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    function IsNumericHarga(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        document.getElementById("errorHarga").style.display = ret ? "none" : "inline";
        return ret;
    }
</script>
@endpush