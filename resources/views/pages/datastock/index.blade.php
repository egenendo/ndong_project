@extends('layouts.app',[
    'activePage'    => 'data_stock',
    'title'         => __('Data Stock')
    ])

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Stock</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Data Stock</li>
                    </ol>
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @include('layouts.notification.partial')
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">List Data Stock</h3>
                            </div>
                            <!-- /.card-header -->
                            
                            <div class="card-body">
                                <a href="{{route('create.data_stock')}}" class="btn btn-success btn-rounded waves-effect waves-light float-sm-right">
                                    <i class="ri-add-circle-line"></i> 
                                    <span>Add Data Stock</span>
                                </a><br><br>
                                <table id="datastockTable" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Gambar</th>
                                            <th>Kode Item</th>
                                            <th>Nama</th>
                                            <th>Stock</th>
                                            <th>created at</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                            </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@push('javascript')
<script>
    $(function () {
        $("#datastockTable").DataTable({
            processing: true,
            serverSide: false,
            ajax: "{{ route('list.data_stock') }}",
            columns: [
                {
                    "data": "id_data_stock",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'images', name: 'images',
                    render: function(data, type, row) {
                        return '<img src="{{asset('')}}'+data+'" style="height:auto;width:120px;"/>';
                    }
                },
                {data: 'code_item', name: 'code_item'},
                {data: 'nama_item', name: 'nama_item'},
                {data: 'stock', name: 'stock'},
                {
                data: 'created_at',
                type: 'num',
                render: {
                    _: 'display',
                    sort: 'timestamp'
                    }
                },
                {data: 'action', name: 'action'},
            ]
        })
    });
</script>
@endpush