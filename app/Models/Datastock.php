<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Datastock extends Model
{
    use Uuids;
    public $incrementing = false;
    public $timestamps = true;
    protected $table = 'data_stock';
    protected $primaryKey = 'id_data_stock';
    protected $fillable = [];

    public function set($data)
    {
        $this->id_kategori = isset($data->id_kategori) ? $data->id_kategori : '';
        $this->code_item = isset($data->code_item) ? $data->code_item : '';
        $this->nama_item = isset($data->nama_item) ? $data->nama_item : '';
        $this->stock = isset($data->stock) ? $data->stock : '';
        if(isset($data->images)){
            $this->images = isset($data->images) ? $data->images : '';
        }
        $this->harga = isset($data->harga) ? $data->harga : '';
        $this->description = isset($data->description) ? $data->description : '';
        return $this;
    }
}
