<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;



class Kategori extends Model
{
    use Uuids;
    public $incrementing = false;
    public $timestamps = true;
    protected $table = 'kategori';
    protected $primaryKey = 'id_kategori';
    protected $fillable = [];

    public function set($data)
    {
        $this->nama_kategori = isset($data->nama_kategori) ? $data->nama_kategori : '';
        
        return $this;
    }
}
