<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;


class History extends Model
{
    use Uuids;
    public $incrementing = false;
    public $timestamps = true;
    protected $table = 'history';
    protected $primaryKey = 'id_history';
    protected $fillable = [];
}
