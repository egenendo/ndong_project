<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\Datastock;
use App\Models\Kategori;
use App\Models\History;

use DataTables;

class DatastockController extends Controller
{
    public function index(Request $request)
    {
        \DB::beginTransaction();
        try {
            \DB::commit();

            $data_stock = Datastock::orderBy('created_at','asc')->get();
            if (!$data_stock) {
                throw new \Exception('Data not found');
            }

            $data = [
                'data_stock' => isset($data_stock) ? $data_stock : null,
            ];
            
            return view('pages.datastock.index',$data);
        } catch (\Throwable $e) {
            dd($e->getMessage());
            \DB::rollback();
            return redirect()->route('home')->with('error', 'Terjadi Kesalahan! (' . $e->getMessage() . ')');
        }
    }

    public function ListDataStock(){
        $data_stock = Datastock::orderBy('created_at','asc');

        return Datatables::of($data_stock)
            ->addIndexColumn()
            ->editColumn('created_at', function ($data_stock) {
                return [
                    'display' => e($data_stock->created_at->format('m/d/Y H:i:s')),
                    'timestamp' => $data_stock->created_at->timestamp
                ];
            })
            ->addColumn('action', function($row){
                $btn = '';
                $btn = '<a href="' . route('edit.data_stock', $row->id_data_stock) . '" class="btn btn-primary btn-rounded waves-effect waves-light"><i class="ri-edit-2-line"></i> Edit</a>' . '&nbsp';
                $btn = $btn.'<a href="'.route('delete.data_stock',$row->id_data_stock).'" class="btn btn-danger btn-rounded waves-effect waves-light" onclick="return confirm(`Are you sure ? `)"><i class="ri-delete-bin-5-line"></i> Delete</a>'.'&nbsp';
                $btn = $btn.'<a href="' . route('index.history', $row->id_data_stock) . '" class="btn btn-warning btn-rounded waves-effect waves-light"><i class="ri-edit-2-line"></i> History</a>' . '&nbsp';
                return $btn;
                })
            ->rawColumns(['action'])
            ->addColumn('status', function($row){
                if($row->status == 1){
                    return 'ACTIVE';
                }else if($row->status == 0){
                    return 'INACTIVE';
                }
                })
            ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(created_at,'%m/%d/%Y') LIKE ?", ["%$keyword%"]);
            })
            ->make(true);
    }

    public function create(Request $request)
    {
        \DB::beginTransaction();
        try {
            \DB::commit();
            $kategori = Kategori::orderBy('created_at','asc')->get();
            if (!$kategori) {
                throw new \Exception('Data not found');
            }

            $data = [
                'kategori' => isset($kategori) ? $kategori : null,
            ];
            
            return view('pages.datastock.create',$data);
        } catch (\Throwable $e) {
            dd($e->getMessage());
            \DB::rollback();
            return redirect()->route('home')->with('error', 'Terjadi Kesalahan! (' . $e->getMessage() . ')');
        }
    }

    public function store(Request $request)
    {
        \DB::beginTransaction();
        try{    
            
            $rules = [                
                'code_item' => 'required',
                'nama_item' => 'required',
                'id_kategori' => 'required',
                'images' => 'mimes:jpg,jpeg,png,pdf|max:1024',
                'harga' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                // dd($validator);
                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $float_stock = 0;
            $data_input = [
                "code_item" => $request->code_item,
                "nama_item" => $request->nama_item,
                "stock" => $float_stock,
                "id_kategori" => $request->id_kategori,
                // "sub_title" => $request->sub_title,
                "images" => '',
                "harga" => $request->harga,
                "description" => $request->description,
            ];
            
            $data_input = (object)$data_input;
            $data_stock = new Datastock;
            $data_stock->set($data_input);
            $data_stock->save();
            
            if($request->file('images')){
                $file = $request->file('images');
                $extension = $file->getClientOriginalExtension();
                $tujuan_upload = 'upload/data_stock/';
                $nama_file = $tujuan_upload.$data_stock->id_data_stock.'.'.$extension;
                $file->move(public_path($tujuan_upload),$nama_file);
                $data_stock->images = $nama_file;
                $data_stock->save();
                // dd('a');
            }
            // dd($data_stock);
            \DB::commit();
            return redirect()->route('index.data_stock')->with('success','Data Stock data saved successfully!');
        }catch(\Throwable $e){
            dd($e->getMessage());
            \DB::rollback();
            return redirect()->route('index.data_stock')->with('error', 'Terjadi Kesalahan! ('.$e->getMessage().')');
        }
    }

    public function edit(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            \DB::commit();
            $data_stock = Datastock::where('id_data_stock',$id)->first();
            if (!$data_stock) {
                throw new \Exception('Data not found');
            }
            $kategori = Kategori::orderBy('created_at','asc')->get();
            if (!$kategori) {
                throw new \Exception('Data not found');
            }
            $data = [
                "data_stock" => $data_stock,
                "kategori" => $kategori,
            ];
            
            return view('pages.datastock.edit',$data);
        } catch (\Throwable $e) {
            dd($e->getMessage());
            \DB::rollback();
            return redirect()->route('home')->with('error', 'Terjadi Kesalahan! (' . $e->getMessage() . ')');
        }
    }

    public function update(Request $request, $id){
        \DB::beginTransaction();
        try{
           
            $rules = [
                'code_item' => 'required',
                'nama_item' => 'required',
                'id_kategori' => 'required',
                'images' => 'mimes:jpg,jpeg,png,pdf|max:1024',
                'harga' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                // dd($validator);
                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            
            $data_stock = Datastock::where('id_data_stock',$id)->first();
            if(!$data_stock){
                throw new \Exception('Banner not found');
            }
            
            $data_input = [
                "code_item" => $request->code_item,
                "nama_item" => $request->nama_item,
                "id_kategori" => $request->id_kategori,
                "harga" => $request->harga,
            ];
            if($request->file('images')){
                if (file_exists(public_path($data_stock->images))) {
                    unlink(public_path($data_stock->images));
                }
                $file = $request->file('images');
                $extension = $file->getClientOriginalExtension();
                $tujuan_upload = 'upload/data_stock/';
                $nama_file = $tujuan_upload.$data_stock->id_data_stock.'.'.$extension;
                $file->move(public_path($tujuan_upload),$nama_file);
                $data_stock->images = $nama_file;
                $data_stock->save();
            }
            $data_input = (object)$data_input;
            $data_stock->set($data_input);
            $data_stock->save();

            \DB::commit();
            return redirect()->route('index.data_stock')->with('success','Data Stock updated successfully!');
        }catch(\Throwable $e){
            // dd($e->getMessage());
            \DB::rollback();
            return redirect()->route('index.data_stock')->with('error', 'Terjadi Kesalahan! ('.$e->getMessage().')');
        }
    }

    public function delete($id){
        \DB::beginTransaction();
        try{
            $data_stock = Datastock::where('id_data_stock',$id)->first();
            if(!$data_stock){
                throw new \Exception('Data Stock not found');
            }
            if (file_exists(public_path($data_stock->images))) {
                unlink(public_path($data_stock->images));
            }
            $data_stock->delete();
            \DB::commit();
            return redirect()->route('index.data_stock')->with('success','Data Stock deleted successfully!');
        }catch(\Throwable $e){
            \DB::rollback();
            return redirect()->route('index.data_stock')->with('error', 'Terjadi Kesalahan! ('.$e->getMessage().')');
        }
    }

    public function index_history(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            \DB::commit();
            $data_stock = Datastock::select(
                'data_stock.*',
                'kategori.nama_kategori',
            )
            ->join('kategori','kategori.id_kategori','data_stock.id_kategori')
            ->where('id_data_stock',$id)
            ->first();
            if (!$data_stock) {
                throw new \Exception('Data not found');
            }

            $history_item = History::select(
                'history.*',
                'data_stock.images',
                'data_stock.code_item',
                'data_stock.nama_item',
                'data_stock.harga',
            )
            ->join('data_stock','data_stock.id_data_stock','history.id_stock')
            ->where('id_stock',$id)
            ->get();
            if (!$history_item) {
                throw new \Exception('Data History not found');
            }
        
            $data = [
                "data_stock" => $data_stock,
                "history_item" => $history_item,
            ];
            

            return view('pages.datastock.index_history',$data);
        } catch (\Throwable $e) {
            dd($e->getMessage());
            \DB::rollback();
            return redirect()->route('home')->with('error', 'Terjadi Kesalahan! (' . $e->getMessage() . ')');
        }
    }

    public function store_history(Request $request,$id)
    {
        \DB::beginTransaction();
        try{    
            
            $rules = [                
                'stock_history' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                // dd($validator);
                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            
            if ($request->status_history == 0) {
                $data_stock = Datastock::where('id_data_stock', $id)->first();
                if (!$data_stock) {
                    throw new \Exception('Data Stock not found');
                }
                if ($data_stock->stock == 0) {
                    return redirect()->route('index.history', $id)->with('warning', 'Stock is empty');
                } elseif ($data_stock->stock < (int) $request->stock_history) {
                    return redirect()->route('index.history', $id)->with('warning', 'Not Enough Stock');
                } else {
                    $stock_inventory = $data_stock->stock;
                    $float_stock_usage = (int) $request->stock_history;
                    $result = $stock_inventory + (-$float_stock_usage);
                    $data_stock->stock = isset($result) ? $result : null;
                    $data_stock->save();

                    $history_inventory = new History;
                    $history_inventory->id_stock = $data_stock->id_data_stock;
                    $history_inventory->id_kategori = $data_stock->id_kategori;
                    $history_inventory->stock_history = isset($float_stock_usage) ? $float_stock_usage : 0;
                    $history_inventory->status_history = isset($request->status_history) ? $request->status_history : 0;
                    $history_inventory->description = isset($request->description) ? $request->description : null;
                    $history_inventory->supplier_name = isset($request->supplier_name) ? $request->supplier_name : null;
                    $history_inventory->save();

                }
            } else {
                $data_stock = Datastock::where('id_data_stock', $id)->first();
                
                if (!$data_stock) {
                    throw new \Exception('Data Stock not found');
                }
                $stock_inventory = $data_stock->stock;
                $float_stock_additions = (int) $request->stock_history;
                $result = $stock_inventory + $float_stock_additions;
                $data_stock->stock = isset($result) ? $result : null;
                $data_stock->save();

                $history_inventory = new History;
                $history_inventory->id_stock = $data_stock->id_data_stock;
                $history_inventory->id_kategori = $data_stock->id_kategori;
                $history_inventory->stock_history = isset($float_stock_additions) ? $float_stock_additions : 0;
                $history_inventory->status_history = isset($request->status_history) ? $request->status_history : 0;
                $history_inventory->description = isset($request->description) ? $request->description : null;
                $history_inventory->supplier_name = isset($request->supplier_name) ? $request->supplier_name : null;
                $history_inventory->save();
                

            }

            // dd($data_stock);
            \DB::commit();
            return redirect()->route('index.history',$id)->with('success','Data Stock data saved successfully!');
        }catch(\Throwable $e){
            dd($e->getMessage());
            \DB::rollback();
            return redirect()->route('index.history',$id)->with('error', 'Terjadi Kesalahan! ('.$e->getMessage().')');
        }
    }
}
