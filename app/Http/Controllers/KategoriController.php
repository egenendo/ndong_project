<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\Kategori;
use DataTables;



class KategoriController extends Controller
{
    
    public function index(Request $request)
    {
        \DB::beginTransaction();
        try {
            \DB::commit();
            $kategori = Kategori::orderBy('created_at','asc')->get();
            if (!$kategori) {
                throw new \Exception('Data not found');
            }

            $data = [
                'kategori' => isset($kategori) ? $kategori : null,
            ];
            
            return view('pages.kategori.index',$data);
        } catch (\Throwable $e) {
            dd($e->getMessage());
            \DB::rollback();
            return redirect()->route('home')->with('error', 'Terjadi Kesalahan! (' . $e->getMessage() . ')');
        }
    }

    public function ListKategori(){
        $kategori = Kategori::orderBy('created_at','asc');

        return Datatables::of($kategori)
            ->addIndexColumn()
            ->editColumn('created_at', function ($kategori) {
                return [
                    'display' => e($kategori->created_at->format('m/d/Y H:i:s')),
                    'timestamp' => $kategori->created_at->timestamp
                ];
            })
            ->addColumn('action', function($row){
                $btn = '';
                $btn = '<a href="' . route('edit.kategori', $row->id_kategori) . '" class="btn btn-primary btn-rounded waves-effect waves-light"><i class="ri-edit-2-line"></i> Edit</a>' . '&nbsp';
                $btn = $btn.'<a href="'.route('delete.kategori',$row->id_kategori).'" class="btn btn-danger btn-rounded waves-effect waves-light" onclick="return confirm(`Are you sure ? `)"><i class="ri-delete-bin-5-line"></i> Delete</a>'.'&nbsp';
                return $btn;
                })
            ->rawColumns(['action'])
            ->addColumn('status', function($row){
                if($row->status == 1){
                    return 'ACTIVE';
                }else if($row->status == 0){
                    return 'INACTIVE';
                }
                })
            ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(created_at,'%m/%d/%Y') LIKE ?", ["%$keyword%"]);
            })
            ->make(true);
    }

    public function create(Request $request)
    {
        \DB::beginTransaction();
        try {
            \DB::commit();
            
            return view('pages.kategori.create');
        } catch (\Throwable $e) {
            dd($e->getMessage());
            \DB::rollback();
            return redirect()->route('home')->with('error', 'Terjadi Kesalahan! (' . $e->getMessage() . ')');
        }
    }

    public function store(Request $request)
    {
        \DB::beginTransaction();
        try{    

            $rules = [                
                'nama_kategori' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                // dd($validator);
                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $data_input = [
                "nama_kategori" => $request->nama_kategori,
                // "sub_title" => $request->sub_title,
            ];

            // dd($data_input);
            $data_input = (object)$data_input;
            $kategori = new Kategori;
            $kategori->set($data_input);
            $kategori->save();
            
            // if($request->file('image')){
            //     $file = $request->file('image');
            //     $extension = $file->getClientOriginalExtension();
            //     $tujuan_upload = 'upload/kategori/';
            //     $nama_file = $tujuan_upload.$kategori->id_kategori.'.'.$extension;
            //     $file->move(public_path($tujuan_upload),$nama_file);
            //     $kategori->image = $nama_file;
            //     $kategori->save();
            // }
            \DB::commit();
            return redirect()->route('kategori')->with('success','Kategori data saved successfully!');
        }catch(\Throwable $e){
            dd($e->getMessage());
            \DB::rollback();
            return redirect()->route('kategori')->with('error', 'Terjadi Kesalahan! ('.$e->getMessage().')');
        }
    }

    public function edit(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            \DB::commit();
            $kategori = Kategori::where('id_kategori',$id)->first();
            if (!$kategori) {
                throw new \Exception('Data not found');
            }
            $data = [
                "kategori" => $kategori,
            ];
            
            return view('pages.kategori.edit',$data);
        } catch (\Throwable $e) {
            dd($e->getMessage());
            \DB::rollback();
            return redirect()->route('home')->with('error', 'Terjadi Kesalahan! (' . $e->getMessage() . ')');
        }
    }

    public function update(Request $request, $id){
        \DB::beginTransaction();
        try{
            $rules = [
                'nama_kategori' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                // dd($validator);
                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            
            $kategori = Kategori::where('id_kategori',$id)->first();
            if(!$kategori){
                throw new \Exception('Kategori not found');
            }
            
            $data_input = [
                "nama_kategori" => $request->nama_kategori,
            ];
            
            $data_input = (object)$data_input;
            $kategori->set($data_input);
            $kategori->save();
            
            \DB::commit();
            return redirect()->route('kategori')->with('success','Kategori data updated successfully!');
        }catch(\Throwable $e){
            // dd($e->getMessage());
            \DB::rollback();
            return redirect()->route('kategori')->with('error', 'Terjadi Kesalahan! ('.$e->getMessage().')');
        }
    }

    public function delete($id){
        \DB::beginTransaction();
        try{
            $kategori = Kategori::where('id_kategori',$id)->first();
            if(!$kategori){
                throw new \Exception('Kategori not found');
            }

            $kategori->delete();
            \DB::commit();
            return redirect()->route('kategori')->with('success','Kategori data deleted successfully!');
        }catch(\Throwable $e){
            \DB::rollback();
            return redirect()->route('kategori')->with('error', 'Terjadi Kesalahan! ('.$e->getMessage().')');
        }
    }
}
