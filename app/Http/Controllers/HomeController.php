<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Kategori;
use App\Models\Datastock;
use App\Models\History;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        // return view('home');
        // return view('welcome');
        \DB::beginTransaction();
        try {
            \DB::commit();
            $user = Auth::user();
            if (!$user) {
                throw new \Exception('News not found');
            }

            $kategori = Kategori::orderBy('created_at','desc')->get()->count();
            if (!$kategori) {
                throw new \Exception('Kategori not found');
            }

            $data_stock = Datastock::orderBy('created_at','desc')->get()->count();
            if (!$data_stock) {
                throw new \Exception('Data Stock not found');
            }

            $history_add = History::where('status_history',1)->get()->count();
            if (!$history_add) {
                throw new \Exception('Data History Add not found');
            }

            $history_use = History::where('status_history',0)->get()->count();
            if (!$history_use) {
                throw new \Exception('Data History Use not found');
            }

            $data = [
                'user' => isset($user) ? $user : null,
                'kategori' => isset($kategori) ? $kategori : 0,
                'data_stock' => isset($data_stock) ? $data_stock : 0,
                'history_add' => isset($history_add) ? $history_add : 0,
                'history_use' => isset($history_use) ? $history_use : 0,
            ];
            // dd($data);
            return view('pages.dashboard.index',$data);
        } catch (\Throwable $e) {
            dd($e->getMessage());
            \DB::rollback();
            return redirect()->route('home')->with('error', 'Terjadi Kesalahan! (' . $e->getMessage() . ')');
        }

        
    }
}
